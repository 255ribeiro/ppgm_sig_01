# Tutorial

## Atividade 1:

### Gerar tabela com lista de coordenadas geográficas e demais atributos para os centroides dos municípios que interceptam a geometria de um bioma em específico.

#### Passos:

1. Abrir arquivos shapefile (Biomas BR e Municípios do Brasil)
2. Avaliar se o SRC são equivalentes / Avaliar a necessidade de mudança do SRC
3. Selecionar o Bioma de interesse utilizando a ferramenta de seleção de atributos via tabela de atributos
4. Salvar feição de interesse (Bioma)
5. Utilizar a ferramenta de seleção por localização para selecionar municípios que interceptam a poligonal do Bioma selecionado
6. Salvar feições de interesse (Municípios)
7. Gerar centroides a partir da camada de municípios salva no passo anterior
8. No shapefile referente aos centroides criar campos de Longitude/Latitude via tabela de atributos e calculadora de campos
9. Salvar edição de inclusão dos atributos Longitude/Latitude
10. Salvar arquivo no formato *.csv (Centroides com coordenadas geográficas)

## Atividade 2

### Recortar arquivos raster baseado em feições de polígonos (extensão / máscara) / Calcular área de classes em arquivos raster.

#### Passos:

1. Abrir arquivos shapefile (Biomas BR e Municípios do Brasil) 
1. Avaliar se o SRC são equivalentes / Avaliar a necessidade de mudança do SRC
1. Selecionar Bioma de interesse utilizando a ferramenta de seleção de atributos via tabela de atributos
1. Salvar feição de interesse (Bioma)
1. Selecionar um munícipio específico dentro do conjunto do a ferramenta de seleção por localização para selecionar municípios que interceptam a poligonal do Bioma selecionado
1. Salvar feições de interesse (Municípios)
2. Recortar a camada raster  MapBiomas 2019 BR  por máscara / extensão com município de interesse
3. Calcular área das classes dos arquivos via ferramenta r.report




## Atividade 3

### Reclassificar arquivos raster (contínuos e categóricos) / Converter arquivo raster em arquivo vetorial.

#### Passos:

1. Abrir os arquivos raster (do arquivo dados prática 3)
1. Observar e entender a distribuição dos dados dos arquivos em termos de variação dafrequência utilizando a ferramenta de histograma e os valores máximos e mínimos (propriedades da camada)
1. Reclassificar os dados de elevação e mapa de uso e cobertura da terra do MapBiomas (recortar pela máscara para algum município da Caatinga)
1. Para reclassificação utilizar as ferramentas r.reclass ou reclassificar por tabela
1. Salvar em arquivo *.txt metadado da reclassificação de cada camada
1. Para ambas as camadas converter pra arquivo vetorial (ferramenta converter)

## Atividade 4

### Converter raster para pontos gerar amostras aleatórias em arquivos vetoriais de pontos gerar interpolação – IDW / TRIN

#### Passos:

1. Abrir arquivo raster (do arquivo dados prática 4)
2. Converter arquivo raster de elevação para pontos (função pixel de raster para ponto)
3. Criar amostras com diferentes porcentagens (10, 20, 30, 40 e 50) do total de pontos do arquivo anterior
4. Gerar as interpolações com os métodos IDW / TRIN
5. Comparar visualmente os resultados com o dado original

