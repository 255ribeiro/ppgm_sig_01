<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="1e+08" version="3.16.7-Hannover" hasScaleBasedVisibilityFlag="0" styleCategories="AllStyleCategories" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <temporal mode="0" fetchMode="0" enabled="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <customproperties>
    <property value="false" key="WMSBackgroundLayer"/>
    <property value="false" key="WMSPublishDataSourceUrl"/>
    <property value="0" key="embeddedWidgets/count"/>
    <property value="Value" key="identify/format"/>
  </customproperties>
  <pipe>
    <provider>
      <resampling zoomedInResamplingMethod="nearestNeighbour" zoomedOutResamplingMethod="nearestNeighbour" enabled="false" maxOversampling="1"/>
    </provider>
    <rasterrenderer alphaBand="-1" type="paletted" opacity="1" nodataColor="" band="1">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <colorPalette>
        <paletteEntry value="3" color="#006400" label=" Formação Florestal" alpha="255"/>
        <paletteEntry value="4" color="#32cd32" label="Formação Savânica" alpha="255"/>
        <paletteEntry value="15" color="#ffd966" label="Pastagem" alpha="255"/>
        <paletteEntry value="21" color="#fff3bf" label=" Mosaico de Agricultura e Pastagens" alpha="255"/>
        <paletteEntry value="24" color="#aa0000" label=" Infraestrutura Urbana" alpha="255"/>
        <paletteEntry value="25" color="#ff99ff" label="Outras Áreas não Vegetadas" alpha="255"/>
        <paletteEntry value="29" color="#ff8c00" label="Afloramento Rochoso" alpha="255"/>
        <paletteEntry value="33" color="#0000ff" label="Rio, Lago e Oceano" alpha="255"/>
      </colorPalette>
      <colorramp type="randomcolors" name="[source]"/>
    </rasterrenderer>
    <brightnesscontrast brightness="0" gamma="1" contrast="0"/>
    <huesaturation colorizeRed="255" colorizeStrength="100" colorizeGreen="128" grayscaleMode="0" saturation="0" colorizeBlue="128" colorizeOn="0"/>
    <rasterresampler maxOversampling="1"/>
    <resamplingStage>resamplingFilter</resamplingStage>
  </pipe>
  <blendMode>2</blendMode>
</qgis>
