\providecommand{\abntreprintinfo}[1]{%
 \citeonline{#1}}
\setlength{\labelsep}{0pt}\begin{thebibliography}{}
\providecommand{\abntrefinfo}[3]{}
\providecommand{\abntbstabout}[1]{}
\abntbstabout{v-1.9.7 }

\bibitem[ActiveConclusion 2020]{ActiveConclusion2020}
\abntrefinfo{ActiveConclusion}{ACTIVECONCLUSION}{2020}
{ACTIVECONCLUSION. \emph{{COVID19 mobility}}. 2020.
Dispon{\'\i}vel em:
  \url{https://github.com/ActiveConclusion/COVID19\_mobility}.}

\bibitem[Bermudez-Edo, Barnaghi e Moessner 2018]{Bermudez-Edo2018}
\abntrefinfo{Bermudez-Edo, Barnaghi e Moessner}{BERMUDEZ-EDO; BARNAGHI;
  MOESSNER}{2018}
{BERMUDEZ-EDO, M.; BARNAGHI, P.; MOESSNER, K. {Analysing real world data
  streams with spatio-temporal correlations: Entropy vs. Pearson correlation}.
\emph{Automation in Construction}, v.~88, n. May 2017, p. 87--100, 2018.
ISSN 09265805.}

\bibitem[Chaturvedi, Toshniwal e Parida 2020]{Chaturvedi2020}
\abntrefinfo{Chaturvedi, Toshniwal e Parida}{CHATURVEDI; TOSHNIWAL;
  PARIDA}{2020}
{CHATURVEDI, N.; TOSHNIWAL, D.; PARIDA, M. \emph{{Harnessing Social
  Interactions on Twitter for Smart Transportation Using Machine Learning}}.
  Springer International Publishing, 2020. v. 584 IFIP. 281--290~p.
ISSN 1868422X.
ISBN 9783030491857. Dispon{\'\i}vel em:
  \url{http://dx.doi.org/10.1007/978-3-030-49186-4\_24}.}

\bibitem[DigitalGeographyLab 2019]{DigitalGeographyLab2019}
\abntrefinfo{DigitalGeographyLab}{DIGITALGEOGRAPHYLAB}{2019}
{DIGITALGEOGRAPHYLAB. \emph{cross border mobility twitter}. 2019.
Dispon{\'\i}vel em:
  \url{https://github.com/DigitalGeographyLab/cross-border-mobility-twitter}.}

\bibitem[IBGE 2018]{IBGE_regic_2018}
\abntrefinfo{IBGE}{IBGE}{2018}
{IBGE. \emph{{REGIC - Regi{\~{o}}es de Influ{\^{e}}ncia das Cidades}}. 2018.
Dispon{\'\i}vel em:
  \url{https://www.ibge.gov.br/geociencias/organizacao-do-territorio/redes-e-fluxos-geograficos/15798-regioes-de-influencia-das-cidades.html?=&t=acesso-ao-produto}.}

\bibitem[IBGE 2020]{IBGE_dtb_2020}
\abntrefinfo{IBGE}{IBGE}{2020}
{IBGE. \emph{{Divis{\~{a}}o Territorial Brasileira}}. 2020.
Dispon{\'\i}vel em:
  \url{https://www.ibge.gov.br/geociencias/organizacao-do-territorio/estrutura-territorial/23701-divisao-territorial-brasileira.html?=&t=downloads}.}

\bibitem[Milusheva et al. 2021]{Milusheva2021}
\abntrefinfo{Milusheva et al.}{MILUSHEVA et al.}{2021}
{MILUSHEVA, S. et al. {Applying machine learning and geolocation techniques to
  social media data (Twitter) to develop a resource for urban planning}.
\emph{PloS one}, v.~16, n.~2, p. e0244317, 2021.
ISSN 19326203.
Dispon{\'\i}vel em: \url{http://dx.doi.org/10.1371/journal.pone.0244317}.}

\bibitem[Oh et al. 2021]{Oh2021}
\abntrefinfo{Oh et al.}{OH et al.}{2021}
{OH, J. et al. {Mobility restrictions were associated with reductions in COVID
  ‑ 19 incidence early in the pandemic : evidence from a real ‑ time
  evaluation in 34 countries}.
p. 1--17, 2021.}

\bibitem[Ribeiro 2018]{Ribeiro2018}
\abntrefinfo{Ribeiro}{RIBEIRO}{2018}
{RIBEIRO, F.~F. \emph{ARQB30 Trabalho Pratico}. 2018.
Dispon{\'\i}vel em:
  \url{https://255ribeiro.github.io/arqb30\_trabalho\_pratico/}.}

\bibitem[{SEFAZ - Prefeitura Municipal de Salvador} 2021]{Cart_Salvador_2021}
\abntrefinfo{{SEFAZ - Prefeitura Municipal de Salvador}}{{SEFAZ - Prefeitura
  Municipal de Salvador}}{2021}
{{SEFAZ - Prefeitura Municipal de Salvador}. \emph{{Mapeamento Cartg{\'{a}}fico
  de Salvador}}. 2021.
Dispon{\'\i}vel em:
  \url{http://mapeamento.salvador.ba.gov.br/geo/desktop/index.html\#on=layer/default;artsic2006/artsic2006;art1k/art1k;eixos/eixos;bairros/bairros;apcp/apcp;scalebar\_meters/scalebar\_m&loc=38.21851414258813;-4290488.480970503;-1447103.1872239653}.}

\bibitem[Szocska et al. 2021]{Szocska2021}
\abntrefinfo{Szocska et al.}{SZOCSKA et al.}{2021}
{SZOCSKA, M. et al. {Countrywide population movement monitoring using mobile
  devices generated (big) data during the COVID-19 crisis}.
\emph{Scientific Reports}, Nature Publishing Group UK, v.~11, n.~1, p.~1--9,
  2021.
ISSN 20452322.
Dispon{\'\i}vel em: \url{https://doi.org/10.1038/s41598-021-81873-6}.}

\bibitem[Tejaswin, Kumar e Gupta 2015]{Tejaswin2015}
\abntrefinfo{Tejaswin, Kumar e Gupta}{TEJASWIN; KUMAR; GUPTA}{2015}
{TEJASWIN, P.; KUMAR, R.; GUPTA, S. {Tweeting traffic: Analyzing twitter for
  generating real-time city traffic insights and predictions}.
\emph{ACM International Conference Proceeding Series}, v. 20-March-2015, 2015.}

\bibitem[Tyrovolas et al. 2021]{Tyrovolas2021}
\abntrefinfo{Tyrovolas et al.}{TYROVOLAS et al.}{2021}
{TYROVOLAS, S. et al. {Estimating the COVID-19 Spread Through Real-time
  Population Mobility Patterns: Surveillance in Low- and Middle-Income
  Countries}.
\emph{Journal of Medical Internet Research}, v.~23, n.~6, p. e22999, 2021.
ISSN 14388871.}

\end{thebibliography}
