\changetocdepth {4}
\babel@toc {brazil}{}\relax 
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}Introdução}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Questões e Hipóteses}{4}{section.1.1}%
\contentsline {section}{\numberline {1.2}Objetivos}{4}{section.1.2}%
\contentsline {section}{\numberline {1.3}Justificativa}{4}{section.1.3}%
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}Metodologia}{6}{chapter.2}%
\contentsline {section}{\numberline {2.1}Fontes de dados}{6}{section.2.1}%
\contentsline {section}{\numberline {2.2}Processo de trabalho}{7}{section.2.2}%
\contentsline {section}{\numberline {2.3}Estratégias de visualização de dados}{8}{section.2.3}%
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}Resultados esperados}{10}{chapter.3}%
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}Considerações finais}{11}{chapter.4}%
\vspace {\cftbeforepartskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Refer\^encias}{12}{chapter*.4}%
