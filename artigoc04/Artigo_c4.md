---
marp: true
theme: gaia
size: 14:10 
paginate: true
header: "PPGM | 2021"

---
<!-- _header: "" -->
<!-- _paginate: false -->
<!-- _backgroundImage: url('img/BG_CICLES8.png') -->
<!-- _class: invert lead -->

<h1 style=" margin: 0px; padding: 0px; font-size: 45px; text-shadow: -5px 5px #555555ff;">Apresentação sobre o artigo: </h1>
<br>
<h1 style="background-color: #88888877; margin: 0px; padding: 0px; font-size: 50px; color: ">The urban digital lifestyle: An analytical framework for placing digital practices in a spatial context and for developing applicable policy </h1>

<br>

<h1 style="margin: 0px; padding: 0px; font-size: 40px; text-shadow: -5px 5px #555555ff;">Alunos: Basílio Fernandez | Fernando Ribeiro</h1>

---
<!-- _backgroundColor: black -->
<!-- _class: invert -->
![bg  100%](./img/papercover.png)

---

<!-- _class: lead -->
# Abstract:

<p style="font-size:25px; text-align: justify; text-justify: auto">While people’s social backgrounds clearly shape their adoption of digital technology and the Internet, their urban lifestyles and place of residence better explain their digital activities when they are online, and how they use technology. Most studies investigating individuals’ use of digitization have neglected the effects of the physical built environment and the daily life of the community. Addressing this gap, this paper places digital practices in the socio-spatial world, and conceptualizes the term “urban digital lifestyle,” which refers to the dynamic re- lationships among three dimensions: (1) the user’s socioeconomic status, (2) the user’s residency, with a focus on the locale’s socio-spatial characteristics, and (3) the user’s digital practices. Empirically, this paper uses a mixture of methods to analyze the digital usage of residents in four neighborhoods in Tel Aviv. The methods used are neighborhood prototype analysis, digital practices survey (n = 490), and spatial and GIS analyses. Although the results may at first glance support the argument that education and socioeconomic status have significant influence on digital practices, these practices also reflect many other factors associated with the urban lifestyle. Thus, locales, places and neighborhoods remain crucial socio-spatial categories that have a major influence on daily life in the digital age.
</p>

---
<!-- _class: lead invert -->
# Keywords:

* Smart cities 
* Digital divide 
* Neighborhood 
* Urban Fabric 
* Community 
* Land uses
  
  <br>
   <a style="display: block; text-align: center;" href="https://www.sciencedirect.com/science/article/pii/S0264275120313263?via%3Dihub">Link permanente do artigo</a>

---
<!-- _header: "" -->
<!-- _backgroundImage: url('img/BG_CICLES8.png') -->
<!-- _class: invert lead -->
<h1 style="background-color: #88888899; margin: 0px; padding: 0px;">INTRODUÇÃO</h1>

---

<h2 style="text-align: center;">Premissas:</h2>

<div style="text-align: justify; text-justify: inter-word;">
  
 <ul style= "font-size: 30px; padding: 0px 100px;position: absolute; left: 0% ;">
    <li style="color: FireBrick">Trabalhos anteriores desconsideram a correlação entre o espaço urbano onde o indivíduo habita e suas atividades online.</li>
    <li style="color: Teal">Enquanto o 'background' social das pessoas influencia a adoção da tecnologia, o estilo de vida urbano e o local onde mora explicam melhor suas atividades online e como eles usam a tecnologia para suas necessidades diárias.</li>
    <li style= "color: ForestGreen"> O conceito de diferenciação digital deveria ter pluralizada, localizada e estabelecida em uma espacialidade melhor definida</li>
    <li style=  "color: Black;"><span style= "font-size: 40px; font-weight: bold"> Este contexto, ainda pouco estudado, é o ponto de partida deste artigo.</span></li>

</ul> 
</div>

---
<!-- _class: invert lead -->
# Estrutura do Artigo
  
  1. Apresentação do conceito de *urban digital lifestyle* e a tensão existente entre as esperas físicas e digitais da vida urbana.
  2. Contextualização do estudo empírico.
  3. Apresentação dos resultados
  4. Conclusão: uma discussão sobre as dinâmicas das práticas digitais na cidade contemporânea. Propondo que estudos investigando diferenciação digitial foquem menos no indivíduo e mais no contexto socio-espacial.

---
<!-- _header: "" -->
<!-- _backgroundImage: url('img/BG_CICLES8.png') -->
<!-- _class: invert lead -->
<h1 style=" background-color: #88888899; margin: 0px; padding: 0px;">1. Urban locales and digital lifestyles in cities</h1>

---
<!-- _backgroundColor: white --> 
<!-- _class: lead -->

<h1 style= "display: block; position: absolute; top: 8%;
  text-align: center; font-size: 40px; ">O termo Urban Digital Lifestyle refere-se as relações dinâmicas envolvendo 3 dimensões:</h1>

<div style="text-align: center; position: absolute; bottom: 55px;">
<img style= " width: 75%" src= "./img/fig01.jpg" />
</div>

<div style= "position: absolute; bottom: 2%;
  left: 5%; font-size: 30px;">Figure 01
</div>

---

<div style="font-weight: bold; font-size: 32px;"> Os estudos sobre diferenciação digital focam nas características do indivíduo(e.g. idade, classe social, nível de educação, salário, etnia, etc). Mas apresentam lacunas quanto ao contexto geoespacial, agrupadas em 3 pontos chaves:</div>

  * Diversidade das localidades urbanas: <span style="font-size: 25px;color: FireBrick">Diferenças políticas, geográficas e culturais entre diferentes localidades das cidades.</span>
  * A variedade de estruturas urbanas e usos do solo em diferentes localidades urbanas: <span style="font-size: 25px;color: FireBrick">Diferenças na estrutura urbana afetam as praticas diárias (digitais ou não) dos habitantes de uma região</span>
  * Estilos de vida, comunidade e pertencimento nas localidades urbanas:<span style="font-size: 25px;color: FireBrick"> Existe uma interface material entre corpo humano e a cidade, e entre os indivíduos e a comunidade.</span>

---
<!-- _header: "" -->
<!-- _backgroundImage: url('img/BG_CICLES8.png') -->
<!-- _class: invert lead -->
<h1 style="background-color: #88888899; margin: 0px; padding: 0px;"> 2. Methodology: demographics of Tel Aviv and its neighborhoods</h1>

---


<!-- _backgroundColor: white-->


<h1 style= "display: block; position: absolute; top: 10%;
  text-align: center; font-size: 43px; ">O planejamento do experimento divide-se em 5 fases:</h1>

<img style="height: 60%; position: absolute; bottom: 15%" src ='./img/fig02.jpg' />

<div style= "position: absolute; bottom: 3%;
  left: 1%; font-size: 30px;">Figure 02
</div>


---

<!-- _class: lead invert  -->

<h4 style= "text-align: center; font-size: 50px;"> Fase 1 </h4> 
<h5 style="text-align: center; font-size: 40px;">Análise das regiões da cidade:</h5>

<br>

<div style= "text-align: justify; text-justify: auto; font-size: 30px;"> As 77 regiões de Tel Aviv foram analisadas para escolher regioes de estudo que fossem capazes de representar protótipos chave dos estilos de vida na cidade. Aspectos socio-econômicos (renda, configuração familiar, etnia ...) e geoespacias (densidade populacional, proximidade do centro, densidade urbana...) foram levados em conta para definir estas áreas.</div>

---
<!-- _backgroundColor:  white -->

<h4 style= "text-align: center; font-size: 45px; margin: 0px;padding: 0px"> Fase 2 </h4> 
<h5 style="text-align: center; font-size: 30px;line-height: 2">Escolha das regiões de estudo:</h5>



<ol style="font-size: 30px;">
<li>Bavli:<span style="font-size: 25px; color: OrangeRed"> urban-family-oriented neighborhood</span></li>
<li>City Center:<span style="font-size: 25px; color: OrangeRed">urban- socially mixed neighborhood</span></li>
<li>Shapira: <span style="font-size: 25px; color: OrangeRed">local-socially mixed neighborhood</span></li>
<li>Ajami: <span style="font-size: 25px; color: OrangeRed">local-ethnic community neighborhood</span></liurban- socially mixed neighborhood>
</ol>


![bg right 80%](./img/fig03.jpg)


<div style= "position: absolute; bottom: 3%;
  right: 0%; font-size: 30px;">Figure 03
</div>

---

<h4 style= "text-align: center; font-size: 45px;"> Fase 3 </h4> 
<h5 style="text-align: center; font-size: 35px;">Análise espacial das localidades escolhidas:</h5>

<ul style="font-size: 30px">
<li>Mapas de estudo utilizando camadas do <a href=https://www.openstreetmap.org >OpenStreetMaps</a>.</li>
<li>Estatísticas, limites, comunidades e serviços da localidade foram extraídos dos dados de GIS da administração municipal de Tel Aviv-Yafo.</li>
<li>Fotos aéreas obtidas através da ESRI.
<li>Um estudo de campo foi realizado para suplementar informações sobre:
  <ul>
  <li>características de gabarito, proximidade e tipologia das habitações; </li>
  <li>o uso de espaços público e privados de comercio e lazer</li>
  <li>a estrutura de transporte para moradores e visitantes</li>
</ul></ul>

---

<!-- _class:  invert -->
<h4 style= "text-align: center; font-size: 45px;"> Fase 4 </h4> 
<h5 style="text-align: justify; text-justify: auto; font-size: 35px;">Uma pesquisa por telefone, com 44 perguntas e 490 entrevistados (que responderam todas as perguntas) foi realizada para obter informações sobre as atividades digitais nas 4 regiões analisadas, incluindo:</h5>

* frequência do uso de *smartphones*
* os propósitos do uso dos serviços *online* 
* a aptidão para o uso dos instrumentos digitais
* questões relativas a privacidade

---
<!-- _class:  lead -->

<h4 style= "text-align: center; font-size: 45px;"> Fase 5 </h4> 

<br>

<h5 style="text-align: justify; text-justify: auto; font-size: 35px;"> Esta fase avaliou as relações entre o status socioeconômico dos usuários, as características de seus locais e suas práticas digitais. Essa fase organizou os dados coletados por bairro para identificar tipos de estilos de vida digitais urbanos. Os resultados foram então usados para conceituar ainda mais a ideia de estilo de vida digital urbano</h5>

---
<!-- _header: "" -->
<!-- _backgroundImage: url('img/BG_CICLES8.png') -->
<!-- _class: invert lead -->
<h1 style="background-color: #88888899; margin: 0px; padding: 0px;"> 3. Results: Socioeconomic status, urban locales, and digital practices</h1>



---
<!-- _backgroundColor: white-->
![bg right 100%](./img/fig04.jpg)

#### Contexto social e geográfico:
- Mais de 75% dos entrevistados em todas as regiões usam o smartpone diariamente.
- Distribuição desequilibrada da rede pública de Wi-Fi.
###### Indicação de que o uso da internet independe da infraestrutura pública.

<div style= "position: absolute; bottom: 3%;
  right: 4%; font-size: 30px;">Figure 04
</div>

---

<!-- _backgroundColor: white-->
![bg left 80%](./img/fig05.jpg)

<div style= "position: absolute; bottom: 3%;
  left: 0%; font-size: 30px;">Figure 05
</div>

#### Contexto fisíco e espacial

Diferenças na desidade do tecido urbano, acesso a serviços como : Educação, saúde, àreas verdes, trasnporte e wi-fi público.
Cores dos mapas: percentual dos que copram online.


---

<!-- _backgroundColor: white-->

#### Contexto da comunidade local

Uso das ferramentas de comunicação digital para:
- comunicação com autoridades
- comunicação com a comunidade.

![bg right 100%](./img/fig06.jpg)

<div style= "position: absolute; bottom: 3%;
  right: 4%; font-size: 30px;">Figure 06
</div>

---
<!-- _backgroundColor: white-->
<h1 style= "position: absolute; top: 0%;left: 16%;
  text-align: center; font-size: 40px; ">Tabela 01: </h1>

![bg 70%](./img/table_01.png)

---
<!-- _backgroundColor: white-->
<h1 style= "position: absolute; top: 0%; left: 16%;
  font-size: 40px; ">Tabela 02: </h1>

![bg 70%](./img/table_02.png)

---
<!-- _class: invert lead -->

### "Therefore, a holistic perspective that combines the material and immaterial by juxtaposing patterns of digital use with the sociophysical characteristics of the environment and social dynamics in space allows digital behavior to be contextualized."(p. 11)



---
<!-- _header: "" -->
<!-- _backgroundImage: url('img/BG_CICLES8.png') -->
<!-- _class: invert lead -->
<h1 style=" background-color: #88888899; margin: 0px; padding: 0px;"> 4. Conclusions: urban digital lifestyle as a framework for placing digital practices in a spatial context</h1>

---
<!-- _class: lead -->
### Conclusões:
O termo "Urban digital lifestyle" é uma categoria que auxilia a justapor diversos parâmetros, em particular o estatu socio-econômico as caracteristicas socioespacias do local e as praticas digitais dos habitantes.

---
<!-- _class: lead  invert -->
### Conclusões:

Essa justaposição ajuda a indetificar:
- Tipos de estilos de vida dgital
- agrupamentos destes estilos de vida no contexto geográfico.


---
<!-- _class: lead  -->
### Conclusões:

Essa identificação é importante nos sentidos de:

- Reconhecer a ligaçõ entre localidades urbanas e práticas digitais.
- Anaizar os urban digital lifestyles em uma cidade.
- Aprmorar políticas publicas que levam em conta a variedade de estilos de urban digital lifestyles.


---
<!-- _header: "" -->
<!-- backgroundColor: SteelBlue-->
<!-- _class: invert lead -->
<!-- _color: white  -->

# FIM
